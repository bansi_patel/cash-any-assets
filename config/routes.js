module.exports = function(app,io){
	app.get('/', function(req, res) {
		res.render('home');
	});

	app.get('/create/:id', function(req,res){
		//var id = Math.round((Math.random() * 1000000));
		var data = req.params
		res.redirect('/chat/'+data.id);
	});

	app.get('/chat/:id', function(req,res) {
		res.render('chat'); // Render the chant.html view
	});
};