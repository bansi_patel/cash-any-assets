var router = require('express').Router();
var Product = require('../models/product');
var ProductImages = require('../models/productImages');
var ProductVideos = require('../models/productVideos');

var moment = require('moment');
var _ = require('underscore');
var fs = require('fs');
var rn = require('random-number')
var lodash = require('lodash');

var options = {
    min: 1,
    max: 1000000000,
    integer: true,
};

/* start product related API */



router.post('/get', function (req, res) {
    var categoryId = req.body.categoryId;
    if (categoryId) {
        Product.find({ categoryId: categoryId }, function (err, productDetail) {
            console.log("productDetail::", productDetail);
            if (productDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.json(productDetail);
            } else {
                res.statusCode = 400;
                res.send("Product not found");
            }
        });
    } else {
        res.send("Category Id is not found ");
    }

})


router.post('/search', function (req, res) {
    var searchText = req.body.searchText;
    var regOutletSearchText = new RegExp(searchText);
    Product.find({})
        .populate({
            path: 'categoryId',
            model: 'Category',
            match: { name: regOutletSearchText }
        })
        .exec(function (err, productDetail) {
            var productArr = productDetail.filter(function(user) {
                return user.categoryId;
              });
            Product.find({ 'name': regOutletSearchText })
            .populate({
                path: 'categoryId',
                model: 'Category',
            })
            .exec(function (err, response) {
            var productObj = lodash.merge(productArr,response);
                if (productObj.length > 0) {
                    if (err) {
                        res.statusCode = 500;
                        res.send(err);
                    }
                    res.statusCode = 200;
                    res.json(productObj);
                } else {
                    res.statusCode = 400;
                    res.send("Data not found");
                }
            });

        });
});

router.post('/upload/video', function (req, res) {
    var ProductVideo = new ProductVideos();
    var fileName = req.body.fileName;
    var extention = req.body.extention;
    var base64 = req.body.base64;

    if (fileName && extention && base64) {
        fileName = fileName.split(' ').join('_') + "_" + rn(options)
        ProductVideo.fileName = fileName;
        //ProductVideo.video = base64;

        var pathimg =  'assets/videos/products/' + fileName + "." + extention;

        var data = base64.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64')
        // to save image into database.
        fs.writeFile(pathimg, buf, function (err) {
            if (err) {
                return res.status(201).json('issue in file upload, ' + err.message);
            } else {
                ProductVideo.path = pathimg;

                ProductVideo.save(function (err, video) {
                    if (err) {
                        res.statusCode = 500;
                        res.json(err);
                    } else {
                        res.statusCode = 201;
                        res.json({ message: 'Product Vedio added successfully', videoId: video._id });
                    }
                })
            }
        });

    } else {
        res.statusCode = 400;
        res.json({ message: 'passed parameter are not valid' });
    }

});


router.get('/all', function (req, res) {
    Product.find({})
        .populate({
            path: 'images',
            model: 'ProductImages',
            options: { select: { path: 1 } }
        })
        .populate({
            path: 'videos',
            model: 'ProductVideos',
            options: { select: { path: 1 } }
        })
        .populate({
            path: 'createdBy',
            model: 'User',
            options: { select: { name: 1 } }
        })
        .exec(function (err, productDetail) {
          //  console.log("productDetail == ", productDetail);
            if (productDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.json(productDetail);
            } else {
                res.statusCode = 400;
                res.send("Product not found");
            }
        });
});


router.post('/insert', function (req, res) {
    var product = new Product();
    var productName = req.body.name;

    if (productName) {
        product.name = productName;
        product.description = req.body.description || "";
        product.location.zipcode = req.body.zipcode;
        product.location.addressline1 = req.body.addressline1 || "";
        product.location.addressline2 = req.body.addressline2 || "";
        product.location.state = req.body.state || "";
        product.location.city = req.body.city || "";
        product.location.country = req.body.country || "";
        product.categoryId = req.body.categoryId || "";
        product.images = req.body.images || [];
        product.videos = req.body.videos || [];
        product.price = req.body.price;
        product.createdBy = req.body.createdBy || "";
        product.updatedBy = req.body.updatedBy || "";
        product.status = req.body.status || "";
        product.duration.startDate = req.body.startDate ? moment(req.body.startDate).format("MM-DD-YYYY HH:mm:ss") : "";
        product.duration.endDate = req.body.endDate ? moment(req.body.endDate).format("MM-DD-YYYY HH:mm:ss") : "";

        product.save(function (err) {
            if (err) {
                res.statusCode = 500;
                res.json(err);
            } else {
                res.statusCode = 201;
                res.json({ message: 'product created successfully' });
            }
        })

    } else {
        res.statusCode = 400;
        res.json({ message: 'passed parameter are not valid' });
    }
});


router.post('/info', function (req, res) {
    if (req.body.productId) {
        Product.findOne({ _id: req.body.productId }, 'name _id description duration price location categoryId status date images', function (err, productDetail) {
            if (productDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.json(productDetail);
            } else {
                res.statusCode = 400;
                res.send("Product not found");
            }
        });
    }
});


router.post('/delete', function (req, res) {
    if (req.body.productId) {
        Product.remove({ _id: req.body.productId }, function (err) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            res.statusCode = 200;
            res.send("Product deleted successfully");
        });
    }
});


router.post('/update', function (req, res) {

    if (req.body.productId) {
        var location = {}, duration = {};
        var updateObject = {}
        if (req.body.name) {
            updateObject["name"] = req.body.name;
        }
        if (req.body.description) {
            updateObject["description"] = req.body.description;
        }
        if (req.body.zipcode) {
            location["zipcode"] = parseInt(req.body.zipcode);
        }

        if (req.body.addressline1) {
            location["addressline1"] = req.body.addressline1;
        }
        if (req.body.addressline2) {
            location["addressline2"] = req.body.addressline2;
        }
        if (req.body.state) {
            location["state"] = req.body.state;
        }
        if (req.body.city) {
            location["city"] = req.body.city;
        }
        if (req.body.country) {
            location["country"] = req.body.country;
        }

        if (req.body.categoryId) {
            updateObject["categoryId"] = req.body.categoryId;
        }

        if (req.body.images) {
            updateObject["images"] = req.body.images;
        }

        if (req.body.videos) {
            updateObject["videos"] = req.body.videos;
        }

        if (req.body.price) {
            updateObject["price"] = parseFloat(req.body.price);
        }

        if (req.body.createdBy) {
            updateObject["createdBy"] = req.body.createdBy;
        }

        if (req.body.updatedBy) {
            updateObject["updatedBy"] = req.body.updatedBy;
        }

        if (req.body.status) {
            updateObject["status"] = req.body.status;
        }

        if (req.body.startDate) {
            duration["startDate"] = req.body.startDate;
        }

        if (req.body.endDate) {
            duration["endDate"] = req.body.endDate;
        }

        if (!_.isEmpty(duration)) {
            updateObject["duration"] = duration;
        }

        if (!_.isEmpty(location)) {
            updateObject["location"] = location;
        }

        Product.update({ _id: req.body.productId }, updateObject, function (err) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            res.statusCode = 200;
            res.json("data update successfully");
        });
    } else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }

});

/*end product related API*/



/*start api for product images*/

router.post('/image/insert', function (req, res) {
    var ProductImage = new ProductImages();
    var fileName = req.body.fileName
    var extention = req.body.extention
    var base64 = req.body.base64
   
    if (fileName && extention && base64) {
        fileName = fileName.split(' ').join('_') + "_" + rn(options)
        ProductImage.fileName = fileName;
     //   ProductImage.image = base64;

        var pathimg =  'assets/images/products/' + fileName + "." + extention;

        var data = base64.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64')
        // to save image into database.
        fs.writeFile(pathimg, buf, function (err) {
            if (err) {
                return res.status(201).json('issue in file upload, ' + err.message);
            } else {
                ProductImage.path = pathimg;

                ProductImage.save(function (err, image) {
                    if (err) {
                        res.statusCode = 500;
                        res.json(err);
                    } else {
                        res.statusCode = 201;
                        res.json({ message: 'product image added successfully', imageId: image._id });
                    }
                })
            }
        });

    } else {
        res.statusCode = 400;
        res.json({ message: 'passed parameter are not valid' });
    }
});

router.post('/image/delete', function (req, res) {
    var imageId = req.body.imageId;
    if (imageId) {
        // here we need to fetch path from database to delete it.
        ProductImages.findOne({ _id: imageId }, 'path', function (err, productImagesData) {
            if (productImagesData) {
                var path = productImagesData.path;
                ProductImages.remove({ _id: req.body.imageId }, function (err) {
                    if (err) {
                        res.statusCode = 500;
                        res.send(err);
                    }

                    // while removed from database here we need to remove from folder structure as well.
                    fs.unlink(path, function (error, success) {
                        if (error) {
                            res.statusCode = 500;
                            console.log("Error in removing image from folder:", error);
                        } else {
                            console.log("Image remove successfully")
                        }
                    })

                });
            } else {
                res.statusCode = 404;
                res.json({ message: 'no such image found' });
            }
        })
    } else {
        res.statusCode = 400;
        res.json({ message: 'passed parameter are not valid' });
    }
});

/*end api for product images*/


module.exports = router;
/* start extra used functions */

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^(.+)$/),
    //var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    console.log("\n decode == ++ \n",response.data);
    return response;
}
/* end extra used functions */