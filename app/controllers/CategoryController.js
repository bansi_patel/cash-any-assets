var router = require('express').Router();
var Category = require('../models/category');

router.get('/all', function all(req, res) {
    Category.find({}, 'name _id', function (err, categoryDetail) {
        console.log("categoryDetail::", categoryDetail);
        if (categoryDetail) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            res.statusCode = 200;
            res.json(categoryDetail);
        } else {
            res.statusCode = 400;
            res.send("Category not found");
        }
    });

})

router.post('/insert', function (req, res) {
    var category = new Category();
    category.name = req.body.name;
    if (category.name) {
        category.save(function (err) {
            if (err) {
                res.statusCode = 500;
                res.json(err);
            } else {
                res.statusCode = 201;
                res.json({ message: 'category created successfully' });
            }
        })
    } else {
        res.statusCode = 400;
        res.json({ message: 'passed parameter are not valid' });
    }
})

router.post('/info', function (req, res) {
        if (req.body.categoryId) {
            Category.findOne({ _id: req.body.categoryId }, 'name _id', function (err, categoryDetail) {
                if (categoryDetail) {
                    if (err) {
                        res.statusCode = 500;
                        res.send(err);
                    }
                    res.statusCode = 200;
                    res.json(categoryDetail);
                } else {
                    res.statusCode = 400;
                    res.send("Category not found");
                }
            });
        }
    });


router.post('/delete', function (req, res) {
        if (req.body.categoryId) {
            Category.remove({ _id: req.body.categoryId }, function (err) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.send("Category deleted successfully");
            });
        }
    });

/*end category related API*/


module.exports = router;