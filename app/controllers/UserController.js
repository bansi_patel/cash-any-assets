var router = require('express').Router();
var User = require('../models/user');
var UserDocuments = require('../models/userDocuments');
var lodash = require('lodash');
var Chats = require('../models/chat');

var fs = require('fs');
var rn = require('random-number')

var options = {
    min: 1,
    max: 1000000000,
    integer: true,
};

// Chat box list wihh last message
router.post('/getChatUserList', function (req, res) {
    if (req.body.currentUserId) {
        var sort = { 'when': 'desc' };
        Chats.find({ $or:  [{"sellerId": req.body.currentUserId}, { "customerId":  req.body.currentUserId }]})
        .populate({
            path: 'by',
            model: 'User',
            options: { select: { name: 1 } }
        })
        .populate({
            path: 'sellerId',
            model: 'User',
            options: { select: { name: 1 } }
        })
        .populate({
            path: 'customerId',
            model: 'User',
            options: { select: { name: 1 } }
        })
        .sort(sort).exec(function (err, chatDetail) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            } else {
                var groupData= lodash.groupBy(chatDetail, function(chat) { 
                    return chat.sellerId
                })
                var data = [];
                lodash.map(groupData, function(v, i) {
                    
                   // console.log(v);
                    data.push(v[0]);
                  //  console.log(groupData);
                });
            //    console.log("chatDetail ",gro);
                res.statusCode = 200;
                res.json(data);
            }
        });
    } else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }
});


/* Logout  */

router.post('/logout', function (req, res) {
    if (req.body.email && req.body.token) {
        User.update({ email: req.body.email, tokens: req.body.token }, { $pull: { 'tokens':  req.body.token } }, function (err, userDetail) {
            if (userDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                } else {
                    res.statusCode = 200;
                    res.json(userDetail);
                }
            } else {
                res.statusCode = 404;
                res.send("User not found");
            }
        });
    } else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }

});

/*start user related API*/
router.post('/upload/document', function (req, res) {
    var UserDocument = new UserDocuments();
    var fileName = req.body.fileName
    var extention = req.body.extention
    var base64 = req.body.base64

    if (fileName && extention && base64) {
        fileName = fileName.split(' ').join('_') + "_" + rn(options)
        UserDocument.fileName = fileName;
        //  UserDocument.image = base64;

        var pathimg = 'assets/images/users/' + fileName + "." + extention;

        var data = base64.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64')
        // to save image into database.
        fs.writeFile(pathimg, buf, function (err) {
            if (err) {
                return res.status(201).json('issue in file upload, ' + err.message);
            } else {
                UserDocument.path = pathimg;

                UserDocument.save(function (err, image) {
                    if (err) {
                        res.statusCode = 500;
                        res.json(err);
                    } else {
                        res.statusCode = 201;
                        res.json({ message: 'User Document added successfully', imageId: image._id });
                    }
                })
            }
        });

    } else {
        res.statusCode = 400;
        res.json({ message: 'passed parameter are not valid' });
    }
});

router.post('/insert', function (req, res) {
    // console.log("req::", req.body);
    var user = new User();
    var tok = [];
    if (req.body.token) {
        var aa = req.body.token;
        tok.push(aa);
    }
    if (req.body && req.body.email && req.body.password) {
        user.name = req.body.name;
        user.phone = req.body.phone;
        user.email = req.body.email;
        user.password = req.body.password;
        user.lat = req.body.lat;
        user.long = req.body.long;
        user.zipCoce = req.body.zipCoce;
        user.registrationType = req.body.registrationType;
        user.socialId = req.body.socialId;
        user.tokens = tok;
        user.images = req.body.images || [];
        user.save(function (err, user) {
            if (err) {
                res.statusCode = 500;
                res.json(err);
            } else {
                res.statusCode = 201;
                res.json({ message: 'user created successfully', userData: user });
            }
        })
    } else {
        res.statusCode = 500;
        res.json(err);
    }
})

router.post('/authenticate', function (req, res) {
    if (req.body.email && req.body.password) {
        User.findOne({ email: req.body.email, password: req.body.password }, function (err, userDetail) {
            if (userDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                } else {
                    var tok = userDetail.tokens || [];
                    if (req.body.token) {
                        tok.push(req.body.token);
                        userDetail.tokens = tok;
                    }
                    User.update({ email: req.body.email }, { $set: { tokens: tok } }, function (error, user) {
                        if (err) {
                            res.statusCode = 500;
                            res.send(error);
                        } else {
                            res.statusCode = 200;
                            res.json(userDetail);
                        }
                    });
                }
            } else {
                res.statusCode = 404;
                res.send("User not found");
            }
        });
    } else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }

});

router.post('/update', function (req, res) {
    if (req.body.userId) {

        console.log(req.body);
        var updateObject = {}
        if (req.body.name) {
            updateObject["name"] = req.body.name;
        }
        if (req.body.phone) {
            updateObject["phone"] = parseInt(req.body.phone);
        }
        if (req.body.zipcode) {
            updateObject["zipcode"] = parseInt(req.body.zipcode);
        }

        if (req.body.lat) {
            updateObject["lat"] = parseFloat(req.body.lat);
        }

        if (req.body.long) {
            updateObject["long"] = parseFloat(req.body.long);
        }
        console.log("updateObject:", updateObject);
        User.update({ _id: req.body.userId }, updateObject, function (err, userDetail) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            res.statusCode = 200;
            res.json("data update successfully");
        });
    } else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }

});

router.post('/info', function (req, res) {
    if (req.body.userId) {
        User.findOne({ _id: req.body.userId }, 'name email _id zipcode lat long phone socialId registrationType', function (err, userDetail) {
            if (userDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.json(userDetail);
            } else {
                res.statusCode = 400;
                res.send("User not found");
            }
        });
    }
    else if (req.body.socialId) {
        User.findOne({ socialId: req.body.socialId }, 'name email _id zipcode lat long phone socialId registrationType', function (err, userDetail) {
            if (userDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.json(userDetail);
            } else {
                res.statusCode = 400;
                res.send("User not found");
            }
        });
    }
    else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }

});


router.post('/delete', function (req, res) {
    if (req.body.userId) {
        User.remove({ _id: req.body.userId }, function (err) {
            if (err) {
                res.statusCode = 500;
                res.send(err);
            }
            res.statusCode = 200;
            res.send("User deleted successfully");
        });
    }
    else if (req.body.socialId) {
        User.findOne({ socialId: req.body.socialId }, 'name email _id zipcode lat long phone socialId registrationType', function (err, userDetail) {
            if (userDetail) {
                if (err) {
                    res.statusCode = 500;
                    res.send(err);
                }
                res.statusCode = 200;
                res.json(userDetail);
            } else {
                res.statusCode = 400;
                res.send("User not found");
            }
        });
    }
    else {
        res.statusCode = 400;
        res.send("Passed parameters are not valid");
    }

});

/*end user related API*/

module.exports = router;