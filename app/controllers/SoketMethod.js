var User = require('../models/user');
var Chats = require('../models/chat');
var lodash = require('lodash');

exports = module.exports = function (io, gravatar) {
	var chat = io.on('connection', (socket) => {
		// When the client emits the 'load' event, reply with the 
		// number of people in this chat room
		socket.on('load', function (data) {
			var room = findClientsSocket(io, data.id);

			var pathArr = data.id.split('-');
			var other = '';
			if (data.locals.currentId == pathArr[2]) {
				other = pathArr[1];
			} else {
				other = pathArr[2];
			}
			if (room.length < 2) {
				User.findOne({ '_id': other }, 'name', function (err, userDetail) {
					socket.username = data.locals.currentId;
					socket.room = data.id;
					// Add the client to the room
					socket.join(data.id);
					if (room.length == 0) {
						var usernames = [];
						usernames.push(data.locals.currentId);
						usernames.push(other);
					} else {
						var usernames = [];
						usernames.push(room[0].username);
						usernames.push(socket.username);
					}
					var sort = { 'when': 'desc' };
					Chats.find({ customerId: pathArr[2], sellerId: pathArr[1], productId: pathArr[0] }).sort(sort).exec(function (err, list) {
						if (err) {
							console.log("Error = =", err);
						}
						// Chats.find({ customerId: pathArr[2], sellerId: pathArr[1], productId: pathArr[0] }, function (err, list) {
						chat.in(data.id).emit('startChat', {
							boolean: true,
							id: data.id,
							users: usernames,
							roomLength: room.length,
							history: list,
							locals: data.locals,
							otherName: userDetail
						});
					});
				});
			}
			else if (room.length >= 2) {
				socket.emit('tooMany', { boolean: true });
			}
		});

		socket.on('getChatHistory', function (data) {
			var pathArr = data.id.split('-');
			var sort = { 'when': 'desc' };
			Chats.find({ customerId: pathArr[2], sellerId: pathArr[1], productId: pathArr[0] }).sort(sort).exec(function (err, list) {
				if (err) {
					console.log("Error = =", err);
				}
				// Chats.find({customerId: pathArr[2],sellerId:pathArr[1],productId:pathArr[0]}, function(err, list){
				socket.emit('historyList', {
					id: data.id,
					history: list
				});
			});
		});

		// Somebody left the chat
		// socket.on('disconnect', function() {
		// 	socket.broadcast.to(this.room).emit('leave', {
		// 		boolean: true,
		// 		room: this.room,
		// 		user: this.username,
		// 	});
		// 	socket.leave(socket.room);
		// });


		// Handle the sending of messages
		socket.on('sendMsg', function (data) {
			var chatData = new Chats();
			var pathArr = data.id.split('-');
			var other = '';
			if (data.locals.currentId == pathArr[2]) {
				other = pathArr[1];
			} else {
				other = pathArr[2];
			}
			chatData.productId = data.locals.productId;
			chatData.sellerId = data.locals.sellerId;
			chatData.customerId = data.locals.customerId;
			chatData.text = data.msg;
			chatData.by = data.locals.currentId;
			chatData.when = new Date();
			chatData.save(function (err, chatDetail) {
				// console.log("chatDetail ", chatDetail);
				if (err) {
					console.log("\n Chat Not Store == ", err);
					socket.emit('sendMsgResolve', { code: 400, locals: err });
				} else {
					var chatObj = {};
					User.findOne({ '_id': other }, 'name', function (err, userDetail) {
						if (err) {
							console.log("\n Chat Not Store == ", err);
							socket.emit('sendMsgResolve', { code: 400, locals: err });
						} else {
							
							var sort = { 'when': 'desc' };
							Chats.find({ customerId: pathArr[2], sellerId: pathArr[1], productId: pathArr[0] }).sort(sort).exec(function (err, list) {
								if (err) {
									console.log("\n Chat Not Store == ", err);
									socket.emit('sendMsgResolve', { code: 400, locals: err });
								} else {
									chatObj.otherName= userDetail;
									data.locals.otherName = userDetail;
									chatObj.history = list;
									chatObj.when = chatDetail.when;
									chatObj.by = chatDetail.by;
									chatObj.text = chatDetail.text;
									chatObj.customerId = chatDetail.customerId;
									chatObj.sellerId = chatDetail.sellerId;
									chatObj.productId = chatDetail.productId;
								//	console.log("chatDetail ",chatObj);

									socket.emit('sendMsgResolve', { code: 200, locals: chatObj });
									//console.log("\n Chate Store Successfully");
									// When the server receives a message, it sends it to the other person in the room.
									socket.broadcast.to(socket.room).emit('receive', { 'id': data.id, 'locals': data.locals, 'msg': data.msg });
								}
							});
						} // else End User
					}); // else End User
				} // else end save chat
			});
		});
	});
};

function findClientsSocket(io, roomId, namespace) {
	var res = [],
		ns = io.of(namespace || "/");    // the default namespace is "/"

	if (ns) {
		for (var id in ns.connected) {
			if (roomId) {
				var index = ns.connected[id].rooms.indexOf(roomId);
				if (index !== -1) {
					res.push(ns.connected[id]);
				}
			}
			else {
				res.push(ns.connected[id]);
			}
		}
	}
	return res;
}