var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productSchema  = new Schema({
    name: String,
    description: String,
    location:  {
      zipcode: Number,
      addressline1: String,
      addressline2: String,
        state: String,
        city: String,
        country: String
    },
    categoryId: String,
    price: Number,
    createdBy: String,
    updatedBy: String,
    status: String,
    duration: { startDate: Date, endDate: Date },
    date: { createdDate: { type: Date, default: Date.now }, modifiedDate: { type: Date, default: Date.now } },
    images: [String], // this will contain image id only.
    videos: Array
})


module.exports = mongoose.model('Product', productSchema);
