var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema  = new Schema({
    name: String,
    phone: Number,
    email: String,
    password: String,
    lat: Number,
    long: Number,
    zipcode: Number,
    registrationType: String,
    socialId: String,
    images: Array,
    tokens: Array
})


module.exports = mongoose.model('User', userSchema);
