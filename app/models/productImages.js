var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productImagesSchema  = new Schema({
  //  image: Buffer,
    fileName: String,
    path: String
})


module.exports = mongoose.model('ProductImages', productImagesSchema);
