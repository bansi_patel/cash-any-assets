var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userDocSchema  = new Schema({
  //  image: Buffer,
    fileName: String,
    path: String
})


module.exports = mongoose.model('UserDocuments', userDocSchema);
