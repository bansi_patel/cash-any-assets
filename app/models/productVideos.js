var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productImagesSchema  = new Schema({
   // video: Buffer,
    fileName: String,
    path: String
})


module.exports = mongoose.model('ProductVideos', productImagesSchema);
