// This file is executed in the browser, when people visit /chat/<random id>
$(function(){
	var ndID = Number(window.location.pathname.match(/\/chat\/(\d+)$/));
	var socket = io();
	var currentUserId = "",
	friend = "", img='',name="";
	if(ndID == 0) {
		currentUserId = "5a48957d7f148607e0599007";
		name="Bansi";
	} else {
		currentUserId = "5a71fb0a10a24429b78a9c54";
		name="milan";
	}
	// cache some jQuery objects
	var section = $(".section"),
	footer = $("footer"),
	chatScreen = $(".chatscreen"),
	noMessages = $(".nomessages"),
	tooManyPeople = $(".toomanypeople"),
	chatNickname = $(".nickname-chat"),
	chatForm = $("#chatform"),
	textarea = $("#message"),
	messageTimeSent = $(".timesent"),
	chats = $(".chats"),
	noMessagesImage = $("#noMessagesImage");

	// on connection to server get the id of person's room
	var socket = io();

	var locals = {
		currentId: currentUserId,
		productId: "5a53b1ccaae0981982eb3415",
		sellerId: "5a71fb0a10a24429b78a9c54",
		customerId: "5a48957d7f148607e0599007"
	};
	var id = locals.productId+'-'+locals.sellerId+'-'+locals.customerId; 

	socket.on('connect', function(){
		console.log("1. connect ");
		socket.emit('load', { 'id' : id, 'locals' : locals });
	});
	socket.on('tooMany', function(data){
		console.log("tooMany ",data);
	});
	socket.on('startChat', function(data){
		console.log("2. startChat With Histroy",data);
		if(data.roomLength == 0) {
			showMessage("youStartedChatWithNoMessages",data);
		}
		else {
			showMessage("heStartedChatWithNoMessages",data);
		}
		chatNickname.text(friend);
	});

	chatForm.on('submit', function(e){
		e.preventDefault();
		// Create a new chat message and display it directly
	//	console.log("Pass Mesage ",textarea.val());
		showMessage("chatStarted");
		if(textarea.val().trim().length) {
			createChatMessage(textarea.val(), name, img, moment());
			scrollToBottom();
			// Send the message to the other person in the chat
			socket.emit('sendMsg', {'id' : id, 'locals' : locals, 'msg': textarea.val()});		
		}		
		textarea.val(""); // Empty the textarea
	});
	// Submit the form on enter
	textarea.keypress(function(e){
		if(e.which == 13) {
			e.preventDefault();
			chatForm.trigger('submit');
		}
	});
	socket.on('sendMsgResolve', function(data){
		console.log("3. SendMsgResolve ",data);
	});

	socket.on('receive', function(data){
		console.log("4. Receive ",data);
		showMessage('chatStarted');
		if(data.msg.trim().length) {
			createChatMessage(data.msg, 'Other Member', img, moment());
			scrollToBottom();
		}
		console.log("\n Now Emit Get Chat History ");
		socket.emit('getChatHistory', { 'id' : id, 'locals' : locals });
	});

	socket.on('historyList', function(data){
		console.log("0. historyList ",data);		
		
	});
	
	// Function that creates a new chat message
	function createChatMessage(msg,user,imgg,now){
		var who = '';
		if(user===name) {	who = 'me';		}
		else {	who = 'you';	}

		var li = $(
			'<li class=' + who + '>'+
				'<div class="image">' +
					'<img src=' + imgg + ' />' +
					'<b></b>' +
					'<i class="timesent" data-time=' + now + '></i> ' +
				'</div>' +
				'<p></p>' +
			'</li>');

		// use the 'text' method to escape malicious user input
		li.find('p').text(msg);
		li.find('b').text(user);

		chats.append(li);

		messageTimeSent = $(".timesent");
		messageTimeSent.last().text(now.fromNow());
	}

	function scrollToBottom(){
		$("html, body").animate({ scrollTop: $(document).height()-$(window).height() },1000);
	}

	function showMessage(status,data){
		// console.log("show message == ",data);
		if(status === "connected"){
			section.children().css('display', 'none');
		}

		else if(status === "personinchat"){
			chatNickname.text(data.user);
		}

		else if(status === "youStartedChatWithNoMessages") {
			noMessages.fadeIn(1200);
			// console.log("data",data);
			// if(data.history.length > 0) {
			// 	var domPrint;
			// 	data.history.forEach(val => {
					
			// 		console.log("val ",val);
			// 	//	createChatMessage(val.text, val.by, '', val.when);
			// 		var li = $(
			// 			'<li class=' + val.by + '>'+
			// 				'<div class="image">' +
			// 					'<img src=' + '' + ' />' +
			// 					'<b></b>' +
			// 					'<i class="timesent" data-time=' + val.when + '></i> ' +
			// 				'</div>' +
			// 				'<p></p>' +
			// 			'</li>');
			
			// 		// use the 'text' method to escape malicious user input
			// 		li.find('p').text(msg);
			// 		li.find('b').text(user);

			// 		messageTimeSent = $(".timesent");
			// 		messageTimeSent.last().text(now.fromNow());
			// 		domPrint.concat(li);
			// 	});				
		
			// 	chats.append(domPrint);
			// 	chatScreen.css('display','block');
			// }
			footer.fadeIn(1200);			
			friend = data.otherName.name;
			noMessagesImage.attr("src",'');
		}

		else if(status === "heStartedChatWithNoMessages") {
			noMessages.fadeIn(1200);
			// if(data.history.length > 0) {
			// 	chatScreen.css('display','block');
			// }
			footer.fadeIn(1200);
			friend = name;
			noMessagesImage.attr("src",'');
		}

		else if(status === "chatStarted"){				
			section.children().css('display','none');
			chatScreen.css('display','block');
		}

		else if(status === "tooManyPeople") {
			section.children().css('display', 'none');
			tooManyPeople.fadeIn(1200);
		}
	}

});
