var express = require('express');
var app = express();
var db = require('./config/connection');
var router = express.Router();
var bodyParser = require('body-parser');
var path = require('path');

//var route = require('./config/routes.js');
//app.use('/caseanyasset/api', router);

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use('/assets', express.static(path.join(__dirname, 'assets')))

// middleware to use for all requests
// app.use(function (error, req, res, next) {
//     // do logging
//     if(error) {
//         console.error(error.stack);
//     }
//     console.log('here we can check for token.');
//     next(); // make sure we go to the next routes and don't stop here
// });

// router.get('/', function (req, res) {
//     res.json('hello welcome to caseany asset api');
// })

var port = process.env.PORT || 4444;

var server = app.listen(port, function (error) {
    if (error) {
        console.log('Unable to listen for connections', error)
    } 
    console.log('Express server listening on port ' + port);
});

var io = require('socket.io')(server)
// Use the gravatar module, to turn email addresses into avatar images:
var gravatar = require('gravatar');

app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));

require('./config/routes')(app, io);
require('./app/controllers/SoketMethod')(io, gravatar);


var UserController = require('./app/controllers/UserController');
app.use('/caseanyasset/api/users', UserController);
var ProductController = require('./app/controllers/ProductController');
app.use('/caseanyasset/api/product', ProductController);
var CategoryController = require('./app/controllers/CategoryController');
app.use('/caseanyasset/api/category', CategoryController);
